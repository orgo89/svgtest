package org.srs.svgtest

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.JavascriptInterface
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Button
import android.widget.Toast
import groovy.transform.CompileStatic

@CompileStatic
class MainActivity extends Activity {

    private WebView webView

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.webView = this.findViewById(R.id.activity_main_webview) as WebView
        this.webView.with {
            webViewClient = new WebViewClient()

            settings.javaScriptEnabled = true
            settings.builtInZoomControls = true
            settings.displayZoomControls = true
        }

        try {
            this.webView.loadUrl 'file:///android_asset/circles.svg'

            this.webView.addJavascriptInterface(new Object() {

                @JavascriptInterface
                void performClick(String selected) {
                    Toast.makeText(MainActivity.this, "Asiento seleccionado: $selected", Toast.LENGTH_SHORT).show()
                }
            }, 'btn')
        } catch (ex) {
            Log.e 'MainActivity', 'Method: onCreate', ex
        }
    }
}
